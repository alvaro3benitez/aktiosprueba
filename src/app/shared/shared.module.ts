import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { MaterialModule } from './material/material.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
  ],
  imports: [
    CommonModule,
    NgxPaginationModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
   
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    MaterialModule,
    NgxPaginationModule,
   
  ]
})
export class SharedModule { }
