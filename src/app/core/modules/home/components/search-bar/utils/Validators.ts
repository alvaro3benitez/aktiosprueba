import { AbstractControl, ValidatorFn } from '@angular/forms';

export function stringValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const value = control.value;

    if (typeof value === 'string') {
      return null;
    } else {
      return { 'notString': true };
    }
  };
}