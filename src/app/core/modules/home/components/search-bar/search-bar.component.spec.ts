import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { GoogleApiServiceService } from '../../../services/google-api-service.service';
import { of } from 'rxjs';
import { SearchBarComponent } from './search-bar.component';
import { Person } from 'src/app/core/models/person';
import { Country } from 'src/app/core/models/country';

describe('SearchBarComponent', () => {
  let component: SearchBarComponent;
  let fixture: ComponentFixture<SearchBarComponent>;
  let googleApiServiceSpy: jasmine.SpyObj<GoogleApiServiceService>;

  beforeEach(async () => {
    const spy = jasmine.createSpyObj('GoogleApiServiceService', ['getPeople', 'getCountries']);

    await TestBed.configureTestingModule({
      declarations: [ SearchBarComponent ],
      providers: [
        FormBuilder,
        { provide: GoogleApiServiceService, useValue: spy },
      ]
    })
    .compileComponents();

    googleApiServiceSpy = TestBed.inject(GoogleApiServiceService) as jasmine.SpyObj<GoogleApiServiceService>;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should create the search form', () => {
    expect(component.searchForm).toBeTruthy();
  });

  describe('searchQuery', () => {
    it('should emit the search results', () => {
      const query = 'test';
      const countries = [{ id: 1, description: 'test', prefix: 1, language: 'en' }];
      const person = { id: 1, name: 'test', surname: 'test', surname2: 'test', phone: 'test', lastModification: 'test', datebirthday: 'test', sex: 'test', 'country-id': 1 };
      const matchingPeople = [person];
    
      spyOn(component.Results, 'emit');
      const result = component.searchQuery(query, countries);
      
      expect(component.Results.emit).toHaveBeenCalledWith({ peopleResults: [], countriesResults: countries });
      
    });
  });
});
