import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  Observable,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  takeUntil,
  Subject,
  forkJoin,
} from 'rxjs';
import { Country } from 'src/app/core/models/country';
import { Person } from 'src/app/core/models/person';
import { Population } from 'src/app/core/models/population';
import { GoogleApiServiceService } from '../../../services/google-api-service.service';
import { stringValidator } from './utils/Validators';
import { Userdata } from '../../../../models/user';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css'],
})
export class SearchBarComponent implements OnInit {
  @Output() Results: EventEmitter<any> = new EventEmitter<any>();

  searchForm: FormGroup;
  persons!: Person[];
  population$!: Observable<Population>;
  datasource$!: Observable<Userdata>;
  countries!: Country[];
  searchResults$!: Observable<Person[]>;
  destroy$: Subject<boolean> = new Subject;

  ngOnInit(): void {
    this.subscribeToSearchInput();
  }
    
  constructor(
    private fb: FormBuilder,
    private googleApiServiceService: GoogleApiServiceService
  ) {
   
    this.searchForm = this.fb.group({
      searchValue: ['', [Validators.required, stringValidator()]],
    });

  }
  
  subscribeToSearchInput() {
    this.searchForm.controls['searchValue'].valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(500),
        takeUntil(this.destroy$),
        switchMap((query: string) => {
          return forkJoin({
            people: this.googleApiServiceService.getPeople(),
            countries: this.googleApiServiceService.getCountries()
          });
        })
      )
      .subscribe(({people, countries}) => {
        this.persons = people.population.person;
        this.countries = countries.data.country ?? [];
        this.searchQuery(
          this.searchForm.controls['searchValue'].value,
          this.countries
        );
      });
  }


  searchQuery(query: string, countries: Country[]): Person[] {
    const queryString = query.toLowerCase();

    const matchingCountries: Country[] =
      this.countries && this.countries.length > 0
        ? this.countries.filter(
            (country: {
              description: string;
              prefix: { toString: () => string | string[] };
              language: { toString: () => string | string[] };
            }) =>
              country.description.toLowerCase().includes(queryString) ||
              country.prefix.toString().includes(queryString) ||
              country.language.toString().includes(queryString)
          )
        : countries;

    const matchingPeople: Person[] =
      this.persons && this.persons.length > 0
        ? this.persons.filter(
            (person) =>
              person.id.toString().includes(queryString) ||
              person.name.toLowerCase().includes(queryString) ||
              person.surname.toLowerCase().includes(queryString) ||
              person.surname2.toLowerCase().includes(queryString) ||
              person.phone.toLowerCase().includes(queryString) ||
              person.lastModification.toLowerCase().includes(queryString) ||
              person.datebirthday.toLowerCase().includes(queryString) ||
              person.sex.toLowerCase().includes(queryString) ||
              matchingCountries.some(
                (country) => country.id === person['country-id']
              )
          )
        : [];

    this.Results.emit({
      peopleResults: matchingPeople,
      countriesResults: countries,
    });
    return matchingPeople;
  }

 
}
