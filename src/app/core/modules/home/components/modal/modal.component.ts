import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent  {

  @Input() user: any; 
  @Input() prefix: number | undefined;
  @Input() country: string | undefined;
  @Output() choose: EventEmitter<any> = new EventEmitter<any>(); 

  closeModal() {
    this.choose.emit(null);
  }
}
