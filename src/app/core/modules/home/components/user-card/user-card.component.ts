import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent {
  @Input() user: any; 
  @Input() prefix: number | undefined;
  @Output() Select: EventEmitter<any> = new EventEmitter<any>(); 

  selectUser() {
    this.Select.emit(this.user); 

  }

}