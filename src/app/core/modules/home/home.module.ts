import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { UserCardComponent } from './components/user-card/user-card.component';
import { ModalComponent } from './components/modal/modal.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './pages/home/home.component';


@NgModule({
    declarations: [
        
        UserCardComponent,
        ModalComponent,
        SearchBarComponent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        SharedModule,  
         FormsModule,
         ReactiveFormsModule,
         HomeRoutingModule
    ],
    exports: []
})
export class HomeModule { }
