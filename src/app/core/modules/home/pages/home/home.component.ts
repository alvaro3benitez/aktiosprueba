import { Component, Input, OnInit } from '@angular/core';
import { Country } from 'src/app/core/models/country';
import { Person } from 'src/app/core/models/person';
import { FormControl } from '@angular/forms';
import { FormGroup} from '@angular/forms';
import {  Observable} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})


export class HomeComponent implements OnInit{

  @Input()
  userLastSelected!: Person; 
  selectedUser: any;
  peopleResults: Person[] =  [];
  countriesResults!: Country[];
  formulario!: FormGroup;
  searchResults$!: Observable<Person[]>;
  countries: Country[] = [];
  persons!: Person[];
  itemsPerPage = 5;
  currentPage = 1;
  totalItems = 0;

  ngOnInit(): void {
    this.formulario = new FormGroup({
      search: new FormControl()
    });
  }

  updateSelectedUser() {
    this.selectedUser = this.userLastSelected;
  }
  
  showUserDetail(user: any) {
    this.selectedUser = user;
  }

  hideUserDetail() {
    this.selectedUser = null;
  }

  onUserSelect(user: any) {
    //console.log('User selected: ', user);
    this.selectedUser = user;
  }

  handleResults(event: {peopleResults: Person[], countriesResults : Country[]} ) {
    this.peopleResults = event.peopleResults;
    this.countriesResults = event.countriesResults;
  }
  getCountryDescriptionById(id: number | undefined): string  | undefined  {
     const countryDescription= this.countriesResults.find((c) => c.id === id)?.description;
     return countryDescription;
  }
  getPhoneById(id: number | undefined): number  | undefined  {
    const countryPhone= this.countriesResults.find((c) => c.id === id)?.prefix;
    return countryPhone;
 }

  get usersToShow(): any[] {

  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  this.totalItems = this.peopleResults.length;
  return this.peopleResults.slice(startIndex, endIndex);
}

}
