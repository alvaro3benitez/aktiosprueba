import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { BaseApiService } from '../../models/base-api-service';
import { Population } from '../../models/population';
import { Userdata } from '../../models/user';
import { AktiosHttpClient } from '../../services/common/aktios-http-client';
import { AppConfigService } from '../../services/common/app-config.service';

@Injectable({
  providedIn: 'root',
})
export class GoogleApiServiceService extends BaseApiService {
  private countriesUrlEndpoint =
    'datasource.json';
  private peopleUrlEndpoint =
    'info-population.json';

    constructor(aktiosHttpClient: AktiosHttpClient, environment : AppConfigService) {
      super(aktiosHttpClient, environment, environment.api.baseApiAktios);
    }

    
  getPeople(): Observable<Population> {
    
    return super.get(this.peopleUrlEndpoint).pipe(
      catchError((error) => {
        return throwError(() => new Error(error));
      })
    );
  }

  getCountries(): Observable<Userdata> {
  
    return super.get(this.countriesUrlEndpoint).pipe(
      catchError((error) => {
        return throwError(() => new Error(error));
      })
    );
  }
}
