import { HttpClient, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AktiosHttpClient extends HttpClient {
  public constructor(handler: HttpHandler) {
    super(handler);
  }

  public override get(url: string, options?: any): Observable<any> {
    return super.get(url, options);
  }

  public override post(url: string, body?: any, options?: any): Observable<any> {
    return super.post(url, body, options);
  }

  public override put(url: string, body: any, options?: any): Observable<any> {
    return super.put(url, body, options);
  }

  public overridedelete(url: string, options?: any): Observable<any> {
    return super.delete(url, options);
  }
}
