import { APP_BASE_HREF } from '@angular/common';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { Inject, Injectable} from '@angular/core';
import { forkJoin } from 'rxjs';
import { ApiConfiguration } from '../../models/apiConfiguration';


@Injectable({ providedIn: 'root' })
export class AppConfigService {
 
  private apiConfiguration!: ApiConfiguration;
  private httpClient: HttpClient;
 
  get api() {
    return this.apiConfiguration;
  }
  constructor(
    private handler: HttpBackend,
    @Inject(APP_BASE_HREF) public baseHref: string
  ) {
    this.httpClient = new HttpClient(this.handler);
  }

  loadAppConfig(): Promise<void> {
    return forkJoin([
      this.loadFile('api/api-config.local.json'),
    ]).toPromise()
      .then((api) => {
        if (api != undefined) {
          this.apiConfiguration = api[0] as ApiConfiguration;
        }
      })
      .catch((error) => {
        console.error('Error fetching config data:', error);
        throw error; 
      });
  }
  loadFile(filePath: string) {
    return this.httpClient.get(`${this.baseHref}assets/config/${filePath}`);
  }
}