import { Country } from "./country";
import { Language } from "./language";
import { Sex } from "./sex";

export interface Userdata {
    data : {
    sex?:      Sex[];
    language?: Language[];
    country?:  Country[];
    }
}