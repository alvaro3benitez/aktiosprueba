import { Person } from "./person";

export interface Population {
    
    population: {
        person: Person[];
    }
}