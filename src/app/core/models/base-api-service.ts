import { Observable } from 'rxjs';
import { AppConfigService } from '../services/common/app-config.service';
import { AktiosHttpClient } from '../services/common/aktios-http-client';

export class BaseApiService {
  private baseUrl: string;
  constructor(
    public aktiosHttpClient: AktiosHttpClient,
  public environment: AppConfigService,
  private baseApi: string
) {
  this.baseUrl = this.environment.api.baseURL + baseApi;
}
  public get(url: string, options?: any): Observable<any> {
    url = this.baseUrl + url;
    return this.aktiosHttpClient.get(url, options);
  }

  public post(url: string, body?: any, options?: any): Observable<any> {
    url = this.baseUrl + url;
    return this.aktiosHttpClient.post(url, body, options);
  }

  public put(url: string, body: any, options?: any): Observable<any> {
    url = this.baseUrl + url;
    return this.aktiosHttpClient.put(url, body, options);
  }

  public delete(url: string, options?: any): Observable<any> {
    url = this.baseUrl + url;
    return this.aktiosHttpClient.delete(url, options);
  }
}
