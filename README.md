# Prueba Aktios
Solución web para la empresa Paint&Point S.L, parte de la prueba técnica frontend para Aktios

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.0.

Download the .zip project or clone it: 

    git clone https://gitlab.com/alvaro3benitez/aktiosprueba

Before start, requires globally-installed node (tested with v16.x) & npm 8.17.0.

    npm install
    npm run test
    npm run build 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use 
    `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run 
    `ng build` 
to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
